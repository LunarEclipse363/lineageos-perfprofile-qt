package com.mckol.losperfprofilequicktile;

import android.graphics.drawable.Icon;
import android.service.quicksettings.*;
import lineageos.power.PerformanceManager;

public class PerfProfileQuickTileService extends android.service.quicksettings.TileService {
    public PerfProfileQuickTileService() {
    }

    @Override
    public void onStartListening() {
        Tile tile = this.getQsTile();
        PerformanceManager perf_manager = PerformanceManager.getInstance(this);
        int profile = perf_manager.getPowerProfile();

        this.updateTile(tile, profile);
    }

    @Override
    public void onClick() {
        Tile tile = this.getQsTile();
        PerformanceManager perf_manager = PerformanceManager.getInstance(this);
        int next_profile = this.getNextProfile(perf_manager.getPowerProfile());

        perf_manager.setPowerProfile(next_profile);
        this.updateTile(tile, next_profile);
    }

    /// Returns id of the next profile to set the device to after clicking on the quick tile
    private int getNextProfile(int profile_id) {
        switch (profile_id) {
            case PerformanceManager.PROFILE_POWER_SAVE:
                return PerformanceManager.PROFILE_BIAS_POWER_SAVE;
            case PerformanceManager.PROFILE_BIAS_POWER_SAVE:
                return PerformanceManager.PROFILE_BALANCED;
            case PerformanceManager.PROFILE_BALANCED:
                return PerformanceManager.PROFILE_BIAS_PERFORMANCE;
            case PerformanceManager.PROFILE_BIAS_PERFORMANCE:
                return PerformanceManager.PROFILE_HIGH_PERFORMANCE;
            case PerformanceManager.PROFILE_HIGH_PERFORMANCE:
                return PerformanceManager.PROFILE_POWER_SAVE;
            default:
                return -1;
        }
    }

    /// Updates the tile state/icon/label with the proper ones for the provided profile id
    private void updateTile(Tile tile, int profile_id) {
        switch (profile_id) {
            case PerformanceManager.PROFILE_POWER_SAVE:
                tile.setState(Tile.STATE_ACTIVE);
                tile.setLabel(getString(R.string.slowest_qt_label));
                tile.setIcon(Icon.createWithResource(this, R.drawable.ic_perf_profile_slowest));
                break;
            case PerformanceManager.PROFILE_BIAS_POWER_SAVE:
                tile.setState(Tile.STATE_ACTIVE);
                tile.setLabel(getString(R.string.slow_qt_label));
                tile.setIcon(Icon.createWithResource(this, R.drawable.ic_perf_profile_slow));
                break;
            case PerformanceManager.PROFILE_BALANCED:
                tile.setState(Tile.STATE_ACTIVE);
                tile.setLabel(getString(R.string.balanced_qt_label));
                tile.setIcon(Icon.createWithResource(this, R.drawable.ic_perf_profile_balanced));
                break;
            case PerformanceManager.PROFILE_BIAS_PERFORMANCE:
                tile.setState(Tile.STATE_ACTIVE);
                tile.setLabel(getString(R.string.fast_qt_label));
                tile.setIcon(Icon.createWithResource(this, R.drawable.ic_perf_profile_fast));
                break;
            case PerformanceManager.PROFILE_HIGH_PERFORMANCE:
                tile.setState(Tile.STATE_ACTIVE);
                tile.setLabel(getString(R.string.fastest_qt_label));
                tile.setIcon(Icon.createWithResource(this, R.drawable.ic_perf_profile_fastest));
                break;
            default:
                tile.setState(Tile.STATE_UNAVAILABLE);
                tile.setLabel(getString(R.string.unsupported_qt_label));
                tile.setIcon(Icon.createWithResource(this, R.drawable.ic_perf_profile_unsupported));
                break;
        }
        tile.updateTile();
    }
}
